package com.NLtest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static java.nio.file.StandardOpenOption.APPEND;

public class Order {

    Scanner sc = new Scanner(System.in);
    String orderSummary = "";

    public void displayAvailableMenu() {
    System.out.println("Choix menu");
    System.out.println("1 - poulet (accompagnement + boisson");
    System.out.println("2 - boeuf (accompagnement sans boisson)");
    System.out.println("3 - végétarien (accompagnement riz + boisson)");
    System.out.println("Que souhaitez-vous comme menu ?");
}



    public String runMenu() {
        int nbMenu = askMenu();
        int nbSide = -1;
        int nbDrink = -1;
        switch (nbMenu) {
        case 1 :
            nbSide = askSide(true);
            nbDrink=askDrink();
            break;
         case 2:
             nbSide=askSide(true);
             break;
         case 3:
             nbSide=askSide(false);
             nbDrink=askDrink();
             break;
        }
        return nbMenu + "," + nbSide + "," + nbDrink + "%n";
    }

    public void runMenus() {
        Path orderPath = Paths.get("order.csv");
        System.out.println("Combien souhaitez vous commander de menus ?");
        int nbMenus=sc.nextInt();
        orderSummary= "Résumé de votre commande : %n";
        for (int i=1; i<=nbMenus; i++){
            orderSummary+= ("Menu :" +i+ "%n");
            String orderLine = runMenu();
            try {
                Files.write(orderPath, String.format(orderLine).getBytes(), APPEND);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
        System.out.println(" ");
        System.out.println(String.format(orderSummary));

    }
    public int askSomething(String category, String[] responses) {
        System.out.println("Choix " + category);
        for (int i = 1; i <= responses.length; i++) {
            System.out.println(i + " - " + responses[i - 1]);
        }
        System.out.println("Que souhaitez vous comme " + category + "?");
        int nbResponse;
        boolean responseIsGood;
        do {
            nbResponse = sc.nextInt();
            if (nbResponse >= 1 && nbResponse <= responses.length) {
                responseIsGood = true;
            } else {
                responseIsGood = false;
            }
            if (responseIsGood == true) {
                String choice = ("Vous avez choisi comme " + category + " : " + responses[nbResponse - 1]);
                orderSummary+=(choice + "%n");
                System.out.println(choice);
            } else {
                boolean isVowel = "aeiouy".contains(Character.toString(category.charAt(0)));
                if (isVowel) {
                    System.out.println("Vous n'avez pas choisi d'" + category + " parmi les choix proposés");
                } else {
                    System.out.println("Vous n'avez pas choisi de " + category + " parmi les choix proposés");
                }
            }
        } while (responseIsGood == false);
        return nbResponse;
    }

    /**
     * Display a question about menu in the standard input, get response and display it
     */
    public int askMenu() {
        String[] menus = {"poulet", "boeuf", "végétarien"};
        int nbMenu = askSomething("menu", menus);
        return nbMenu;
    }

    /**
     * Display a question about side in the standard input, get response and display it
     */
    public int askSide(boolean allSidesEnable) {
        if (allSidesEnable) {
            String[] responsesAllSide = {"légumes frais", "frites", "riz"};
            return askSomething("accompagnement", responsesAllSide);
        } else {
            String[] responsesOnlyRice = {"riz", "pas de riz"};
            return askSomething("accompagnement", responsesOnlyRice);
        }
    }

    /**
     * Display a question about drink in the standard input, get response and display it
     */
    public int askDrink() {
        String[] responsesDrink = {"eau plate", "eau gazeuse", "soda"};
        return askSomething("boisson", responsesDrink);
    }

}

